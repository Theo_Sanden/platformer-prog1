﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

    // Use this for initialization
    GameObject turretGO;
    GameObject turretChildGo;
    GameObject player;
    float speed = 10f;
    RaycastHit2D playerHit;
    RaycastHit2D otherHit;
    public LayerMask playerMask;
    public LayerMask otherMask;
    Player playerComponents;
    public float bulletDamage = 1;
    private float timer;
	void Start() { 
        player = GameObject.Find("Player");
        playerComponents = player.GetComponent<Player>();
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        transform.position += transform.right * speed * Time.deltaTime;
        playerHit = Physics2D.Raycast(transform.position, transform.right, 0.1f, playerMask);
        Debug.DrawRay(transform.position, transform.right * 0.1f);
        otherHit = Physics2D.Raycast(transform.position, transform.right, 0.1f, otherMask);

        if (playerHit)
        {
            Destroy(this.gameObject);
            playerComponents.damagePlayer(bulletDamage);
        }
        else if (otherHit)
        {

            Destroy(this.gameObject);
        }
        if(timer > 4)
        {
            Destroy(this.gameObject);
        }
    }
}
