﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class NmeHealthbar : MonoBehaviour {

    GameObject attachGo;
    TurretScript turretComponents;
    private float healthBarChange;
    Camera camera;
    private Slider slider;
    public float offSet; 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (attachGo != null)
        {
            healthBarChange = turretComponents.health / turretComponents.startHealth;
            transform.position = camera.WorldToScreenPoint(attachGo.transform.position + new Vector3(0, offSet));
            slider.value = healthBarChange;
            Debug.DrawLine(camera.ScreenToWorldPoint(this.transform.position), attachGo.transform.position);
        }
        else
        {
            Destroy(this.gameObject);
    
        }


                                                       
	}
    public void attachHealthBarTo(GameObject healthBarAttachObject)
    {
        attachGo = healthBarAttachObject;
        turretComponents = healthBarAttachObject.GetComponent<TurretScript>();

        camera = Camera.FindObjectOfType<Camera>();
        slider = this.GetComponent<Slider>();
    }
    public void destroy()
    {
        Destroy(this.gameObject);
    }
}
