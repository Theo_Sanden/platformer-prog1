﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour {
    Transform canvas;
    public int restartCount = 0;
    public GameObject turretPrefab;
    public GameObject healthPrefab;
    GameObject[] turretGOPosition;
    Vector3[] turretPosition;
    List<GameObject> turretObjects = new List<GameObject>();
    List<GameObject> hpObjects = new List<GameObject>();
    List<NmeHealthbar> hpScript = new List<NmeHealthbar>();
    bool instantiateCrosshairOnce;
    public bool connectedController;
    bool connectedControllerBoolLastFrame;
	// Use this for initialization
	void Start () {
        turretGOPosition = GameObject.FindGameObjectsWithTag("TP");
        turretPosition = new Vector3[turretGOPosition.Length];
        int i = 0;
        foreach(GameObject turretPositionGO in turretGOPosition)
        {
            turretPosition[i] = turretPositionGO.transform.position;
         i++;
        }
        Restart();
        connectedController = (Input.GetJoystickNames()[0] == "Controller (XBOX 360 For Windows)") ? true : false;


    }
	
	// Update is called once per frame
	void Update ()
    {
        connectedController = (Input.GetJoystickNames()[0] == "Controller (XBOX 360 For Windows)") ? true : false;
        if (connectedController != connectedControllerBoolLastFrame)
        {
            ChangeCrosshair();
        }
        connectedControllerBoolLastFrame = connectedController;



    }
    public void ChangeCrosshair()
    {
        if (connectedController)
        {
            if (GameObject.Find("CrossHair(Clone)") != null)
            {
                Destroy(GameObject.Find("CrossHair(Clone)"));
                
            }

            Destroy(GameObject.Find("ControlerCrosshair(Clone)"));
            Instantiate(Resources.Load("ControllerCrosshair"));
            print("StansinController");
        }
        else if (!connectedController)
        {
            if (GameObject.Find("ControllerCrosshair(Clone)") != null)
            {
                Destroy(GameObject.Find("ControllerCrosshair(Clone)"));
                
            }

            Destroy(GameObject.Find("CrossHair(Clone)"));
            Instantiate(Resources.Load("CrossHair"));
            print("Stansin");

        }

    }
    public void Restart()
    {
        if (turretObjects.Count != 0)
        {
            foreach (GameObject turret in turretObjects)
            {
                Destroy(turret);
            }
        }
        hpObjects.Clear();
        hpScript.Clear();
        turretObjects.Clear();

        for (int i = 0; i < turretPosition.Length; i++)
        {
            GameObject newTurret = Instantiate(turretPrefab, turretPosition[i], Quaternion.identity);
            turretObjects.Add(newTurret);
        }
        for (int i = 0; i < turretPosition.Length; i++)
        {
            canvas = GameObject.Find("Canvas").transform;
            GameObject newHealthBar = Instantiate(healthPrefab, canvas);
            hpObjects.Add(newHealthBar);
            hpScript.Add(newHealthBar.GetComponent<NmeHealthbar>());
        }
        for (int i = 0; i < turretPosition.Length; i++)
        {

            hpScript[i].attachHealthBarTo(turretObjects[i]);
            turretObjects[i].name = "" + i;
            hpObjects[i].name = "" + i + i;

        }
    }
        public void experienceDrop(int amount, Vector3 position)
        { 

        for (int i = 0; i < amount; i++)
        {
            Instantiate(Resources.Load("ExpPoints"), position, Quaternion.identity);
        }
        }
    }
   

