﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretScript : MonoBehaviour
{
    #region Variables
    GameObject thePlayer;
    GameObject bullet;
    Player player;
    private float distanceToPlayer;
    Vector3 playerPosition;
    private Animator turretAnimator;
    RaycastHit2D aiDetectPlayer;
    public LayerMask playerMask;
    Transform firePoint;
    private float cooldown = 2f;
    private float bulletTimer;
    private bool aggro;
    private int aggroCheck;
    public float health;
    public float startHealth = 30.0f;
    private Transform thisTransform;
    #endregion
    // Use this for initialization
    void Start()
    {

        thePlayer = GameObject.Find("Player");
        player = thePlayer.GetComponent<Player>();
        health = startHealth;
        thisTransform = this.transform;

    }

    // Update is called once per frame
    void Update()
    {
        distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
        turretAnimator = this.GetComponent<Animator>();
        playerPosition = player.transform.position;
        if(distanceToPlayer < 10)
        {
            Debug.DrawLine(transform.position, playerPosition);
            aiDetectPlayer = Physics2D.Linecast(transform.position, playerPosition, playerMask);
            RotateTowardsPlayer(aiDetectPlayer);
        }
        else if (distanceToPlayer > 10)
        {
            aggro = false;
        }       
        if (aggro)
        bulletTimer += Time.deltaTime;
        if (bulletTimer > cooldown)
        {
            bulletTimer = 0;

        }
        if (bulletTimer == 0 && aggro)
        {

            FirePointScript child = this.GetComponentInChildren<FirePointScript>();
            turretAnimator.SetTrigger("Shoot");
            child.Invoke("instantiateBullet", 0.3f);
        }
    }
    private void RotateTowardsPlayer(RaycastHit2D ray)
    {
        if (ray.collider.tag == "Player" && ray.collider.tag != "Obstacle")
        {
            aggro = true;
            transform.right = playerPosition - transform.position;

        }
    }
    public void damageHealth(int damage)
    {
        health -= damage;
        if(health <= 0)
        {
            int random = Random.Range(6, 11);
            print(random);
            GameObject.Find("SceneManager").GetComponent<SceneManager>().experienceDrop(random, transform.position);
            Destroy(this.gameObject);
        }
    }
}
