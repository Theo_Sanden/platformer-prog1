﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePointScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void instantiateBullet()
    {
        GameObject player = GameObject.Find("Player");
        Transform playerTransform = player.GetComponent<Transform>();
        Instantiate(Resources.Load("Bullet"), new Vector3(transform.position.x, transform.position.y, 0), Quaternion.RotateTowards(transform.rotation, playerTransform.rotation, 0));
    }
}
