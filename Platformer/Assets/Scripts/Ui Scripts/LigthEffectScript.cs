﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LigthEffectScript : MonoBehaviour {
    float cos = 1;
    float sin = 1;
    float radius = 0.4f;
    LineRenderer lineRenderer;
    Vector3 orbPosition;
    [SerializeField]
    Gradient[] colorGradient;

    // Use this for initialization
    void Start () {
        lineRenderer = this.GetComponent<LineRenderer>();

	}
	
	// Update is called once per frame
	void Update () {

    
        if (Input.GetAxis("DirX") != 0)
        {
            cos = Input.GetAxis("DirX");

        }
        if (Input.GetAxis("DirY") != 0)
        {
            sin = Input.GetAxis("DirY");

        }

        orbPosition = GameObject.Find("Staff").transform.position + new Vector3(0, 0.5f, 0);
        transform.position = orbPosition + new Vector3(cos, sin).normalized * radius;
        transform.up =  transform.position - orbPosition;
        if (Input.GetKey(KeyCode.JoystickButton4))
        {
            lineRenderer.startWidth = 0.2f;
            lineRenderer.endWidth = 0.2f;
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, orbPosition + new Vector3(cos, sin).normalized * 10);
            lineRenderer.colorGradient = colorGradient[0];
        }
        else
        {
            lineRenderer.startWidth = 0.2f;
            lineRenderer.endWidth = 0.2f;
            lineRenderer.SetPosition(1, orbPosition + new Vector3(cos, sin).normalized * 0.2f);
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.colorGradient = colorGradient[1];

        }
		
	}
}