﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class StartScreenScript : MonoBehaviour {
    public GameObject start;
    public GameObject instructions;
    public GameObject quit;
    private TextMesh startMesh;
    private TextMesh instructionsMesh;
    private TextMesh quitMesh;
    private int textSelector;
    private bool spasmfix;
    private bool haltSelection = false;
    private bool releaseEnter;
    
	// Use this for initialization
	void Start () {
        Cursor.visible = false;
        startMesh = start.GetComponent<TextMesh>();
        instructionsMesh = instructions.GetComponent<TextMesh>();
        quitMesh = quit.GetComponent<TextMesh>();
   
	}
	
	// Update is called once per frame
	void Update () {
        print(Input.GetAxisRaw("Vertical"));
        if (Input.GetKey(KeyCode.Return) != true && Input.GetKey(KeyCode.JoystickButton0) != true)
        {
            releaseEnter = true;
        }
        if (!haltSelection)
        {
            if (Input.GetAxisRaw("Vertical") == 0 && Input.GetAxisRaw("Vertical") == 0)
                spasmfix = true;

            if (Input.GetAxisRaw("Vertical") <  0 && spasmfix)
            {
                textSelector++;
                spasmfix = false;
            }
            if (Input.GetAxisRaw("Vertical") > 0 && spasmfix)
            {
                textSelector--;
                spasmfix = false;
            }
        


            if (textSelector == 3)
                textSelector = 0;
            if (textSelector == -1)
                textSelector = 2;

            if (textSelector == 1)
            {


                startMesh.color = Color.grey;
                instructionsMesh.color = Color.white;
                quitMesh.color = Color.grey;
            }
            else if (textSelector == 0)
            {


                instructionsMesh.color = Color.grey;
                startMesh.color = Color.white;
                quitMesh.color = Color.grey;
            }
            else if (textSelector == 2)
            {

                startMesh.color = Color.grey;
                instructionsMesh.color = Color.grey;
                quitMesh.color = Color.white;

            }



            if ((Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.JoystickButton0)) && textSelector == 0 && releaseEnter)
            {
                releaseEnter = false;
                UnityEngine.SceneManagement.SceneManager.LoadScene("MainCamera");
            }
            if ((Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.JoystickButton0)) && textSelector == 1 && releaseEnter)
            {
                releaseEnter = false;
                UnityEngine.SceneManagement.SceneManager.LoadScene("Instructions");
            }
            if ((Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.JoystickButton0)) && textSelector == 2 && releaseEnter)
            {
                Instantiate(Resources.Load("BoardThingy"));
                haltSelection = true;
                releaseEnter = false;

            }

            
        }
        if (haltSelection)
        {
            GameObject yes = GameObject.Find("Yes");
            GameObject no = GameObject.Find("No");
            TextMesh yesMesh = yes.GetComponent<TextMesh>();
            TextMesh noMesh = no.GetComponent<TextMesh>();
            if (Input.GetAxisRaw("Horizontal") == -1 && spasmfix)
            {
                spasmfix = false;
                textSelector--;
            }
            print(spasmfix);
            if (Input.GetAxisRaw("Horizontal") == 1 && spasmfix)
            {
                spasmfix = false;
                textSelector++;
            }
            if (Input.GetAxisRaw("Horizontal") == 0 )
            {
                spasmfix = true;
            }
            textSelector = (textSelector == 2) ? 0 : (textSelector == -1) ? 1 : textSelector;
            if (textSelector == 1)
            {
                yesMesh.color = Color.grey;
                noMesh.color = Color.white;
            }
            if (textSelector == 0)
            {
                yesMesh.color = Color.white;
                noMesh.color = Color.grey;
            }
            print(releaseEnter);
            if ((Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.JoystickButton0)) && textSelector == 0 && releaseEnter)
            {
                Application.Quit();
            }
            if((Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.JoystickButton0)) && textSelector == 1 && releaseEnter)
            {
                releaseEnter = false;
                Destroy(GameObject.Find("BoardThingy(Clone)"));
                print("Im releasing");
                haltSelection = false;
            }
        }
    }
}
