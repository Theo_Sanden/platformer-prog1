﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {
    public Transform playerPosition;
    public float smoothDampTime = 0.01f;

    public float cameraOffset;
    private Vector3 velocity;
    public bool cameraJitter = false;
	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (cameraJitter)
        {
            updatePosition();
        }
    }
    void FixedUpdate () {

        if (!cameraJitter)
        {
            updatePosition();
        }
       
    }
    void updatePosition()
    {
        if (playerPosition.position.x < 0)
            transform.position = Vector3.SmoothDamp(transform.position, playerPosition.position - new Vector3(cameraOffset, 0), ref velocity, smoothDampTime);

        else
            transform.position = Vector3.SmoothDamp(transform.position, playerPosition.position - new Vector3(cameraOffset * -1, 0), ref velocity, smoothDampTime);

        transform.Translate(Vector3.back);

        if(transform.position.y < 0)
        {
            transform.position = new Vector3(transform.position.x, 0,-10);
        }

    }
    
}
