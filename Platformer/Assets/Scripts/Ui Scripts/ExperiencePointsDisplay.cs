﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ExperiencePointsDisplay : MonoBehaviour {
    private int points;
    private Text pointsDisplay;
	// Use this for initialization
	void Start () {
        pointsDisplay = this.GetComponent<Text>();
	}
	
	// Update is called once per frame
	public void addPoints(int refPoints)
    {
        points += refPoints;
        pointsDisplay.text = "Arcane Essence: " + points;


    }

}
