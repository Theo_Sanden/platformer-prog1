﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ActiveWeaponDisplayScript : MonoBehaviour {

    public Sprite[] Icons;
    

    public void ChangeIcon (int IconIndex)
    {
        Image icon = this.GetComponentInChildren<Image>();
        icon.sprite = Icons[IconIndex];
        print("Changing icon");
        
    }
}
