﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class PerkTree : MonoBehaviour {
    private bool show = false;
    public Material offMaterial;
    public Material onMaterial;
    private Image imageComponent;
	// Use this for initialization
	void Start () {
        imageComponent = GetComponent<Image>();

	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (show == true)
                show = false;
            else if( show == false)
                show = true;
        }
        if (!show)
        {
            imageComponent.material = offMaterial;
        }
        else
            imageComponent.material = onMaterial;
            

		
	}
}
