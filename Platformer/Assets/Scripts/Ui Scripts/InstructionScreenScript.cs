﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionScreenScript : MonoBehaviour {
    public GameObject back;
    public GameObject keyboard;
    public GameObject controller;
    private TextMesh backMesh;
    private TextMesh keyboardMesh;
    private TextMesh controllerMesh;
    private int textSelector = 1;
    private bool spasmFix;
    public GameObject controllerText;
    private TextMesh controllerTextMesh;
    public GameObject conText;
    public GameObject keyText;
    private TextMesh conTextMesh;
    private TextMesh keyTextMesh;
	// Use this for initialization
	void Start ()
    {
        backMesh = back.GetComponent<TextMesh>();
        conTextMesh = conText.GetComponent<TextMesh>();
        keyTextMesh = keyText.GetComponent<TextMesh>();
        keyboardMesh = keyboard.GetComponent<TextMesh>();
        controllerMesh = controller.GetComponent<TextMesh>();
        controllerTextMesh = controllerText.GetComponent<TextMesh>();
        Cursor.visible = false;

    }
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetAxisRaw("Horizontal") == 1 && spasmFix)
        {
            textSelector++;
            spasmFix = false;
        }
        if(Input.GetAxisRaw("Horizontal") == -1 && spasmFix)
        {
            textSelector--;
            spasmFix = false;
        }
        if (textSelector > 2)
            textSelector = 1;
        if (textSelector < 0)
            textSelector = 2;
        if (Input.GetAxisRaw("Horizontal") != 1 && Input.GetAxisRaw("Horizontal") != -1)
            spasmFix = true;
        
        if(textSelector == 1)
        {
            backMesh.color = Color.grey;
            keyboardMesh.color = Color.white;
            controllerMesh.color = Color.grey;
            controllerTextMesh.text = keyTextMesh.text;
        }
        if (textSelector == 2)
        {
            backMesh.color = Color.grey;
            keyboardMesh.color = Color.grey;
            controllerMesh.color = Color.white;
            controllerTextMesh.text = conTextMesh.text;
        }
        if (textSelector == 0)
        {
            backMesh.color = Color.white;
            keyboardMesh.color = Color.grey;
            controllerMesh.color = Color.grey;
        }
        if (((Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.JoystickButton0)) && textSelector == 0) || (Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.JoystickButton1)))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("StartScreen");
        }

    }
}
