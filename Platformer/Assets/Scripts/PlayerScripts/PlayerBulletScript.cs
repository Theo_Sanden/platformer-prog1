﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerBulletScript : MonoBehaviour {
    private Vector3 mousePosition;
    private float speed = 20;
    private RaycastHit2D hit;
    public LayerMask hitMask;
    [SerializeField]
    public string[] Nmetags;
    public int bulletDamage;
    private float timer;
	// Use this for initialization
	void Start () {
        bool connectedController = GameObject.Find("SceneManager").GetComponent<SceneManager>().connectedController;
        if (!connectedController)
        {
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePosition.z = 0;
            transform.right = mousePosition - transform.position;
            bulletDamage = 1;

        }
        else if (connectedController)
        {
            transform.right = GameObject.Find("ControllerCrosshair(Clone)").transform.up;
        }
    }
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
       transform.position += transform.right * Time.deltaTime * speed;
       hit = Physics2D.Raycast(transform.position, transform.right, 0.1f, hitMask);
        Debug.DrawRay(transform.position, transform.right * 0.1f, Color.red);

        if (hit)
        {
            foreach (string tag in Nmetags)
            {
                if (hit.collider.tag == tag)
                {
                    if (tag == "Turret")
                    {
                        GameObject.Find(hit.collider.name).GetComponent<TurretScript>().damageHealth(bulletDamage);
                        print("Hitting turret but retarded");

                    }

                }
            }
            Destroy(this.gameObject);
        }
        if(timer > 4)
        {
            Destroy(this.gameObject);
        }

        
     
        
        		
	}
   
}
