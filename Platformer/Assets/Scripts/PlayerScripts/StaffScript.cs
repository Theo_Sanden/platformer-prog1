﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaffScript : MonoBehaviour {
    private Transform playerTransform;
    private float direction;
    private Vector3 staffOffset = new Vector3(1,0,0);
    private float timeReg = 0.2f;
    private float distance;
    private Vector3 refVector = new Vector3(0, 0, 0);
    private float time;
   
	// Use this for initialization
	void Start () {
      
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        direction = GameObject.Find("Player").GetComponent<Player>().direction;
        playerTransform = GameObject.Find("Player").transform;
        if(direction < 0)
        {
            time = 0;
            distance = Vector3.Distance(transform.position, -staffOffset);
            time += Time.deltaTime / timeReg;
            transform.position = Vector3.SmoothDamp(transform.position, playerTransform.position - staffOffset, ref refVector, time);

        }
        if (direction > 0)
        {
            time = 0;
            distance = Vector3.Distance(transform.position, staffOffset);
            time += Time.deltaTime / timeReg;
            transform.position = Vector3.SmoothDamp(transform.position, playerTransform.position + staffOffset, ref refVector, time);

        }
        transform.position = new Vector3(transform.position.x, playerTransform.position.y);
      

            
    }
    public void instantiateBullet()
    {
        bool usingController = GameObject.Find("SceneManager").GetComponent<SceneManager>().connectedController;

        if (!usingController)
        {
            float radius = 0.3f;
            float hypotenuse = Vector3.Distance(transform.position + new Vector3(0, 0.5f, 0), Camera.main.ScreenToWorldPoint(Input.mousePosition));
            float adjacent = transform.position.x - Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
            float opposite = transform.position.y - Camera.main.ScreenToWorldPoint(Input.mousePosition).y;
            float cos = (adjacent / hypotenuse);
            float sin = (opposite / hypotenuse);
            Vector3 instantiatePosition = transform.position + new Vector3(0, 0.5f, 0) + ((new Vector3(-cos, -sin).normalized * radius));
            Instantiate(Resources.Load("PlayerBullet"), instantiatePosition, Quaternion.identity);


        }
        else if(usingController)
        {
            Transform controllerCrosshairTransform = GameObject.Find("ControllerCrosshair(Clone)").transform;
            controllerCrosshairTransform.rotation = new Quaternion(0, 0, controllerCrosshairTransform.rotation.z, 0);
            Instantiate(Resources.Load("PlayerBullet"), controllerCrosshairTransform.position, Quaternion.identity);
        }

       
    }
}
