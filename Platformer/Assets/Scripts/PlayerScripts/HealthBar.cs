﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class HealthBar : MonoBehaviour {
    private GameObject player;
    private Player playerComponents;
    Vector3 playerPosition;
    public Vector3 offset;
    public Camera camera;
    private float healthbarChange;
    public Slider healthBar;

    
	// Use this for initialization
	void Start () {
        player = GameObject.Find("Player");
        playerComponents = player.GetComponent<Player>();
	}
	
	// Update is called once per frame
	void Update () {
        healthbarChange = playerComponents.health / playerComponents.startHealth;
        playerPosition = player.transform.position + offset;
        transform.position = camera.WorldToScreenPoint(playerPosition);
        healthBar.value = healthbarChange; 
	}
}
