﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Player : MonoBehaviour {
    #region Variables
    #region JumpVars
    public LayerMask groundmask;
    public LayerMask altJumpMask;
    Rigidbody2D playerRigid;
    float addForceJump = 300f;
    private float maxJump = 6;
    float addJump = 1.5f;
    bool hasjumped = false;
    bool extraJump;
    bool grounded;
    Vector3 skinWidth = new Vector3(0, -0.5f, 0);
    #endregion
    #region PunchVars
    private bool hasReleasedPunchButton = true;
    private bool hasPunched;
    private float cooldown = 0.2f;
    private float timer;
    #endregion
    public int selectWeapon = 1;
    private float speed = 230f;
    private Animator trampAnim;
    private Animator myAnimator;
    private SpriteRenderer mySpriteRenderer;
    float playerWidth;
    float playerHeight;
    public int damage = 2;
    private int doubleJump = 0;
    public GameObject sceneManager;
    private SceneManager sceneManagerComponents;
    public Transform Spawn;
    private BoxCollider2D collPlayer;
    public Vector3 RayOffset;
    private Vector3 collOffset;
    private Vector3 headOffset;
    public Vector3 RayOffsetPosition;
    private bool instantiatedWeapon = false;
    MeshRenderer playerMesh;
    bool ConnectedController;
    [HideInInspector]
    public float health;
    [HideInInspector]
    public float startHealth= 20.0f;
    [HideInInspector]
    public Vector2 currentPosition;
    [HideInInspector]
    public int direction;
    bool hasReleasedSwitchWeapon;
    public int weaponAmount = 2;
    private bool skipFirstFrame = true;
    private ActiveWeaponDisplayScript ActivateWeaponDisplay;
    #endregion
    #region Raycasts
    RaycastHit2D groundedDownMiddle;
    RaycastHit2D groundedDownLeft;
    RaycastHit2D groundedDownRight;
    RaycastHit2D wallRightTop;
    RaycastHit2D wallRightMiddle;
    RaycastHit2D wallRightBottom;
    RaycastHit2D wallLeftTop;
    RaycastHit2D wallLeftBottom;
    RaycastHit2D wallLeftMiddle;
    RaycastHit2D altJumpDownMiddle;
    RaycastHit2D altJumpDownRight;
    RaycastHit2D altJumpDownLeft;
    List<RaycastHit2D> groundedList = new List<RaycastHit2D>();
    List<RaycastHit2D> wallList = new List<RaycastHit2D>();
    #endregion

    

    // Use this for initialization
    void Start() {
        
        playerRigid = this.GetComponent<Rigidbody2D>();
        playerMesh = this.GetComponent<MeshRenderer>();
        myAnimator = this.GetComponent<Animator>();
        transform.position = Spawn.position;
        mySpriteRenderer = this.GetComponent<SpriteRenderer>();
      //  playerMesh.material.color = Color.blue;
        sceneManager = GameObject.Find("SceneManager");
        sceneManagerComponents = sceneManager.GetComponent<SceneManager>();
        health = startHealth;
        ActivateWeaponDisplay = GameObject.Find("Canvas").GetComponentInChildren<ActiveWeaponDisplayScript>();
        

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetAxisRaw("SwitchWeapons") > 0 && hasReleasedSwitchWeapon)
        {
            selectWeapon++;
            hasReleasedSwitchWeapon = false;
            instantiatedWeapon = false;
            
            

        }
        else if(Input.GetAxisRaw("SwitchWeapons")< 0 && hasReleasedSwitchWeapon)
        {
            selectWeapon--;
            hasReleasedSwitchWeapon = false;
            instantiatedWeapon = false;
            

        }
        else if (Input.GetAxisRaw("SwitchWeapons") == 0)
        {
            hasReleasedSwitchWeapon = true;
        }
        if (selectWeapon > weaponAmount-1)
            selectWeapon = 0;
        else if (selectWeapon < 0)
            selectWeapon = weaponAmount -1;

        print(Input.GetAxisRaw("SwitchWeapons"));
        ConnectedController = (Input.GetJoystickNames()[0] == "Controller (XBOX 360 For Windows)") ? true : false;
     

        
        #region WeaponSelection
        string instantiateWeapon = (selectWeapon == 1) ? "Staff" : "Hit";
       

        if (instantiateWeapon == "Staff" && !instantiatedWeapon)
        {
            ActivateWeaponDisplay.ChangeIcon(selectWeapon);
            Instantiate(Resources.Load(instantiateWeapon), transform.position, Quaternion.identity);
            GameObject.Find(instantiateWeapon + "(Clone)").name = instantiateWeapon;
            instantiatedWeapon = true;           
           if (GameObject.Find("ControllerCrosshair(Clone)") == null && !skipFirstFrame)
               GameObject.Find("SceneManager").GetComponent<SceneManager>().ChangeCrosshair();
        }
        else if (instantiateWeapon == "Hit" && !instantiatedWeapon)
        {
            ActivateWeaponDisplay.ChangeIcon(selectWeapon);
            Destroy(GameObject.Find("CrossHair(Clone)"));
            Destroy(GameObject.Find("ControllerCrosshair(Clone)"));
            print("Destroying crosshair");
            Destroy(GameObject.Find("Staff"));
            instantiatedWeapon = true;

        }
        
        
            

        #endregion
        collOffset = this.GetComponent<BoxCollider2D>().offset;
        //headOffset = this.GetComponent<CircleCollider2D>().offset;
        if (grounded || altJumpDownRight || altJumpDownMiddle || altJumpDownLeft)
        {
          
            myAnimator.SetBool("Falling", false);
            myAnimator.SetBool("Grounded", true);
            if (playerRigid.velocity.x < 0 || playerRigid.velocity.x > 0)
            {
                myAnimator.SetBool("Walking", true);
            }
            else
            {
                myAnimator.SetBool("Walking", false);

            }
        }
        else if (!grounded)
        {
            myAnimator.SetBool("Walking", false);
            myAnimator.SetBool("Grounded", false);
            if (playerRigid.velocity.y < 0)
            {
                myAnimator.SetBool("Falling", true);

            }


        }

        if (direction == -1)
        {
            mySpriteRenderer.flipX = true;
            if (collOffset.x < 0)
            {
                this.GetComponent<BoxCollider2D>().offset = new Vector2(collOffset.x * -1, collOffset.y);
                //this.GetComponent<CircleCollider2D>().offset = new Vector2(headOffset.x * -1, headOffset.y);
                print("Im doing stuff");
                RayOffsetPosition.x *= -1;
            }
             
        }
        if (direction == 1)
        {
            mySpriteRenderer.flipX = false;
            if (collOffset.x > 0)
            {
                print("Im doing stuff");
               // this.GetComponent<CircleCollider2D>().offset = new Vector2(headOffset.x * -1, headOffset.y);
                this.GetComponent<BoxCollider2D>().offset = new Vector2(collOffset.x * -1, collOffset.y);
                RayOffsetPosition.x *= -1;
            }
               
            
        }  
        if (hasPunched)
        {
            timer += Time.deltaTime;
        }
        if(timer > cooldown)
        {
            timer = 0;
            hasPunched = false;
        }
        currentPosition = new Vector2(transform.localPosition.x, transform.localPosition.y);

        if (Input.GetAxisRaw("Jump") > 0 && (grounded) && !hasjumped && !extraJump)
        {
            doubleJump = 1;
            playerRigid.AddForce(new Vector2(0, addForceJump));
            hasjumped = true;
        }
        else if (Input.GetAxisRaw("Jump") > 0 && !grounded && !hasjumped && extraJump && (altJumpDownLeft || altJumpDownMiddle || altJumpDownRight))
        {
            doubleJump = 1;
            if (altJumpDownLeft == true)
            trampAnim = GameObject.Find(altJumpDownLeft.collider.name).GetComponent<Animator>();
            else if(altJumpDownMiddle == true)
            trampAnim = GameObject.Find(altJumpDownMiddle.collider.name).GetComponent<Animator>();
            else if(altJumpDownRight == true)
            trampAnim = GameObject.Find(altJumpDownRight.collider.name).GetComponent<Animator>();
            if(trampAnim != null)
            {
                trampAnim.SetTrigger("JumpedOn");
            }
            playerRigid.AddForce(new Vector2(0, addForceJump * addJump));
            hasjumped = true;
        }
        if (Input.GetAxisRaw("Jump") > 0 && !grounded && !hasjumped && doubleJump == 1)
        {
            playerRigid.velocity = new Vector3(playerRigid.velocity.x, 0, 0);
            playerRigid.AddForce(new Vector2(0, addForceJump));
            doubleJump = 0;
            myAnimator.SetBool("Falling", false);
            myAnimator.SetBool("Grounded", true);
            hasjumped = true;
        }
       
        else if (Input.GetAxis("Jump") == 0)
            hasjumped = false;

        if (Input.GetAxis("Fire2") != 0 && hasReleasedPunchButton && !hasPunched)
        {
            if(selectWeapon == 0)
            {
                Instantiate(Resources.Load(instantiateWeapon), currentPosition, Quaternion.identity);

            }
            if(selectWeapon == 1)
            {
                GameObject.Find("Staff").GetComponent<StaffScript>().instantiateBullet();
            }
            hasReleasedPunchButton = false;
            hasPunched = true;
        }
        else if (Input.GetAxis("Fire2") == 0)
        {

            hasReleasedPunchButton = true;
        }
        if (health <= 0)
        {
            Respawn();
        }
        skipFirstFrame = false;
    }
    void FixedUpdate()
    { 
        if (playerRigid.velocity.y > maxJump && !extraJump)
        {
            playerRigid.velocity = new Vector2(playerRigid.velocity.x, maxJump);

        }
        else if (playerRigid.velocity.y > maxJump * addJump && extraJump)
        {
            playerRigid.velocity = new Vector2(playerRigid.velocity.x, maxJump * addJump);

        }

        #region DrawRaycasts
        groundedDownLeft = Physics2D.Raycast(transform.localPosition + RayOffsetPosition + new Vector3(-RayOffset.x, -RayOffset.y, 0), -transform.up, 0.05f, groundmask);
        Debug.DrawRay(transform.localPosition + RayOffsetPosition + new Vector3(-RayOffset.x, -RayOffset.y), -transform.up * 0.05f);
        groundedDownMiddle = Physics2D.Raycast(transform.localPosition + RayOffsetPosition + new Vector3(0,-RayOffset.y,0), -transform.up, 0.05f, groundmask);
        Debug.DrawRay(transform.localPosition + RayOffsetPosition + new Vector3(0, -RayOffset.y, 0), -transform.up * 0.05f);
        groundedDownRight = Physics2D.Raycast(transform.localPosition + RayOffsetPosition + new Vector3(RayOffset.x, -RayOffset.y, 0), -transform.up, 0.05f, groundmask);
        Debug.DrawRay(transform.localPosition + RayOffsetPosition + new Vector3(RayOffset.x, -RayOffset.y, 0), -transform.up * 0.05f);

        wallRightTop = Physics2D.Raycast(transform.localPosition + RayOffsetPosition + new Vector3(RayOffset.x, RayOffset.y, 0), transform.right, 0.05f, groundmask);
        Debug.DrawRay(transform.localPosition + RayOffsetPosition + new Vector3(RayOffset.x, RayOffset.y, 0), transform.right * 0.05f);
        wallRightMiddle = Physics2D.Raycast(transform.localPosition + RayOffsetPosition + new Vector3(RayOffset.x, 0, 0), transform.right, 0.05f, groundmask);
        Debug.DrawRay(transform.localPosition + RayOffsetPosition + new Vector3(RayOffset.x, 0, 0), transform.right * 0.05f);
        wallRightBottom = Physics2D.Raycast(transform.localPosition + RayOffsetPosition + new Vector3(RayOffset.x, -RayOffset.y, 0), transform.right, 0.05f, groundmask);
        Debug.DrawRay(transform.localPosition + RayOffsetPosition + new Vector3(RayOffset.x, -RayOffset.y, 0), transform.right * 0.05f);

        wallLeftTop = Physics2D.Raycast(transform.localPosition + RayOffsetPosition + new Vector3(-RayOffset.x, RayOffset.y, 0), -transform.right, 0.05f, groundmask);
        Debug.DrawRay(transform.localPosition + RayOffsetPosition + new Vector3(-RayOffset.x, RayOffset.y, 0), -transform.right * 0.05f);
        wallLeftMiddle = Physics2D.Raycast(transform.localPosition + RayOffsetPosition + new Vector3(-RayOffset.x, 0, 0), -transform.right, 0.05f, groundmask);
        Debug.DrawRay(transform.localPosition + RayOffsetPosition + new Vector3(-RayOffset.x, 0, 0), -transform.right * 0.05f);
        wallLeftBottom = Physics2D.Raycast(transform.localPosition + RayOffsetPosition + new Vector3(-RayOffset.x, -RayOffset.y, 0), -transform.right, 0.05f, groundmask);
        Debug.DrawRay(transform.localPosition + RayOffsetPosition + new Vector3(-RayOffset.x, -RayOffset.y, 0), -transform.right * 0.05f);

        altJumpDownLeft = Physics2D.Raycast(transform.localPosition + RayOffsetPosition + new Vector3(-RayOffset.x, -RayOffset.y, 0), -transform.up, 0.05f, altJumpMask);
        Debug.DrawRay(transform.localPosition + RayOffsetPosition + new Vector3(-RayOffset.x, -RayOffset.y, 0), -transform.up * 0.05f);
        altJumpDownMiddle = Physics2D.Raycast(transform.localPosition + RayOffsetPosition + new Vector3(0,-RayOffset.y), -transform.up, 0.05f, altJumpMask);
        Debug.DrawRay(transform.localPosition + RayOffsetPosition + new Vector3(0,-RayOffset.y), -transform.up * 0.05f);
        altJumpDownRight = Physics2D.Raycast(transform.localPosition + RayOffsetPosition + new Vector3(RayOffset.x, -RayOffset.y, 0), -transform.up, 0.05f, altJumpMask);
        Debug.DrawRay(transform.localPosition + RayOffsetPosition +  new Vector3(RayOffset.x, -RayOffset.y, 0), -transform.up * 0.05f);



        groundedList.Add(groundedDownLeft);
        groundedList.Add(groundedDownMiddle);
        groundedList.Add(groundedDownRight);
        wallList.Add(wallLeftBottom);
        wallList.Add(wallLeftMiddle);
        wallList.Add(wallLeftTop);
        wallList.Add(wallRightBottom);
        wallList.Add(wallRightMiddle);
        wallList.Add(wallRightTop);
        #endregion
        #region CheckIfGrounded
        int i = 0;
        foreach (RaycastHit2D Ray in groundedList)
        {

            if (Ray == true)
            {
                grounded = true;
                extraJump = false;
            }
            if (Ray == false)
            {
                i++;

            }
            if (i == 3)
                grounded = false;
        }
        i = 0;
        groundedList.Clear();
        if(altJumpDownLeft || altJumpDownMiddle || altJumpDownRight)
        {
            extraJump = true;
        }
        #endregion
        
        
        
        float velocityX = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        Direction(velocityX);
        if(wallLeftBottom || wallLeftMiddle || wallLeftTop)
        {
            
            velocityX = (velocityX < 0) ? 0 : velocityX;
        }
        else if(wallRightBottom || wallRightMiddle|| wallRightTop)
        {
            velocityX = (velocityX < 0) ? velocityX : 0 ;
        }
        
        playerRigid.velocity = new Vector2(velocityX, playerRigid.velocity.y);

    }
    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Kill")
        {
            Respawn();

        }
    }
    void Direction(float directionX)
    {
        if(directionX < 0)
        {
            direction = -1;
           
        }
        
        
        else if( directionX > 0)
        {
            direction = 1;

        }
      
            

    }
    public void damagePlayer(float damage)
    {
        health -= (damage > 0) ? damage : health;
    }
    void Respawn()
    {
        sceneManagerComponents.Restart();
        transform.position = Spawn.position;
        health = startHealth;
    }
   
}
