﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFirePointScript : MonoBehaviour {
    private Vector3 mousePosition;
    private Vector3 staffPosition;
    // Use this for initialization

	void Start () {

       transform.position += new Vector3(0, 0.5f, 0);

    }
	
	// Update is called once per frame
	void Update () {
        staffPosition = GameObject.Find("Staff").transform.position; 
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = 0;
        transform.right = (transform.position - mousePosition) * -1;
        if(Vector3.Distance(staffPosition + new Vector3(0,0.5f,0), transform.position) < 0.5)
        {
           transform.position += transform.right * Time.deltaTime;
        }
      
       

        //transform.rotation = new Quaternion(0,0,transform.rotation.z,0);


    }
}
