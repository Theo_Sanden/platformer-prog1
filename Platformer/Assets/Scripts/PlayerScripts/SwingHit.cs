﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwingHit : MonoBehaviour {
    private float radius = 2f;
    private float angle;
    float speed = 1000f;
    private float direction;
    GameObject thePlayer;
    MeshRenderer hitMesh;
    Player player;
    public LayerMask nmeMask;
    bool hit= false;
    bool hitOnce = false;
    public Vector3 offset;
    public Vector3 offsetOffset;
    private Vector3 rayDirectionX;
    private BoxCollider2D boxColl;
    private SpriteRenderer hitSpriteRenderer;
    // ( ಠ ͜つ ಠ ) 
    RaycastHit2D[]intRay = new RaycastHit2D[6];
    #region raycasts;
    RaycastHit2D rightTopRay;
    RaycastHit2D rightMiddleRay;
    RaycastHit2D rightBottomRay;
    RaycastHit2D bottomRightRay;
    RaycastHit2D bottomMiddleRay;
    RaycastHit2D bottomLeftRay;
    #endregion
    // Use this for initialization
    void Awake () {
        thePlayer = GameObject.Find("Player");
        boxColl = this.GetComponent<BoxCollider2D>();
        hitSpriteRenderer = this.GetComponent<SpriteRenderer>();
        player = thePlayer.GetComponent<Player>();
       // hitMesh = this.GetComponent<MeshRenderer>();
        //hitMesh.material.color = Color.blue;
        direction = player.direction;
        angle = (direction == -1) ? -30 : 30;
        transform.position = player.transform.position;
        if (direction == -1)
        {
            print("Left");
            hitSpriteRenderer.flipX = true;
            rayDirectionX = -transform.right;
            offset.x *= -1;
            offsetOffset *= -1;
            print(rayDirectionX);
            if (boxColl.offset.x > 0)
                boxColl.offset = new Vector3(boxColl.offset.x * -1, boxColl.offset.y);
        }
        else if (direction == 1)
        {
            print("Right");
            hitSpriteRenderer.flipX = false;
            rayDirectionX = transform.right;
            print(rayDirectionX);
            if (boxColl.offset.x < 0)
                boxColl.offset = new Vector3(boxColl.offset.x * -1, boxColl.offset.y);
        }

    }
	
	// Update is called once per frame
	void Update () {
      



        angle = (direction == -1) ? angle + (-speed * Time.deltaTime) : angle + (speed * Time.deltaTime);;

        if (angle > 150 || angle < -150)
        {

            Destroy(this.gameObject);
        }
        transform.localPosition = new Vector3( Mathf.Sin(angle * Mathf.Deg2Rad) + player.currentPosition.x, Mathf.Cos(angle * Mathf.Deg2Rad) + player.currentPosition.y);
       
        #region Raycasts
        rightTopRay = Physics2D.Raycast(transform.position + offsetOffset + new Vector3(offset.x, offset.y), rayDirectionX,0.1f , nmeMask);
        Debug.DrawRay(transform.position + offsetOffset + new Vector3(offset.x, offset.y),rayDirectionX * 0.1f);
        rightMiddleRay = Physics2D.Raycast(transform.position + offsetOffset + new Vector3(offset.x, 0), rayDirectionX, 0.1f, nmeMask);
        Debug.DrawRay(transform.position + offsetOffset + new Vector3(offset.x, 0), rayDirectionX * 0.1f);
        rightBottomRay = Physics2D.Raycast(transform.position + offsetOffset + new Vector3(offset.x, -offset.y), rayDirectionX, 0.1f, nmeMask);
        Debug.DrawRay(transform.position + offsetOffset + new Vector3(offset.x, -offset.y), rayDirectionX * 0.1f);
        bottomRightRay = Physics2D.Raycast(transform.position + offsetOffset + new Vector3(offset.x, -offset.y), -transform.up, 0.1f, nmeMask);
        Debug.DrawRay(transform.position + offsetOffset + new Vector3(offset.x, -offset.y), -transform.up * 0.1f);
        bottomMiddleRay = Physics2D.Raycast(transform.position + offsetOffset + new Vector3(0, -offset.y), -transform.up, 0.1f, nmeMask);
        Debug.DrawRay(transform.position + offsetOffset + new Vector3(0, -offset.y), -transform.up * 0.1f);
        bottomLeftRay = Physics2D.Raycast(transform.position + offsetOffset + new Vector3(-offset.x, -offset.y), -transform.up, 0.1f, nmeMask);
        Debug.DrawRay(transform.position + offsetOffset + new Vector3(-offset.x, -offset.y), -transform.up * 0.1f);
        intRay[0] = rightTopRay;
        intRay[1] = rightMiddleRay;
        intRay[2] = rightBottomRay;
        intRay[3] = bottomRightRay;
        intRay[4] = bottomMiddleRay;
        intRay[5] = bottomLeftRay;
        #endregion  
        int i = 0;
        foreach(RaycastHit2D ray in intRay)
        {
           if(ray == false)
            {
                i++;
            }
            if (i == intRay.Length)
            {
                hitOnce = false;
            }
        }i = 0;
            do
            {
                if (intRay[i] != false && intRay[i].collider.tag == "Turret" && !hitOnce)
                {
                    string name = intRay[i].collider.name;
                    GameObject hitObject = GameObject.Find(name);
                    TurretScript getDamageMethod = hitObject.GetComponent<TurretScript>();
                    getDamageMethod.damageHealth(player.damage);
                    hit = true;
                    hitOnce = true;
                }
                i++;
            } while (!hit && i == 5);
        
    
        


    }
}
