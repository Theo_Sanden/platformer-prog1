﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpScripts : MonoBehaviour {
    private int force = 1;
    private Rigidbody2D thisRigid;
    private float randomAngleX;
    private float randomAngleY;
    private GameObject player;
    private float lerpTime = 0.1f;
    public LayerMask playerMask;
    private Vector3 refVelocity= new Vector3(0,0,0);

    // Use this for initialization
    void Start () {
        randomAngleX = Random.Range(-1.0f, 1.0f);
        randomAngleY = Random.Range(-1.0f, 1.0f);
        thisRigid = this.GetComponent<Rigidbody2D>();
        thisRigid.AddForce(new Vector3(randomAngleX,randomAngleY) * force);
        player = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {
     
        
		
	}
    void FixedUpdate()
    {
       if(Vector3.Distance(player.transform.position, transform.position) < 2)
        {

           thisRigid.simulated = false;
           transform.position = Vector3.SmoothDamp(transform.position, player.transform.position, ref refVelocity, lerpTime);

        if(Vector3.Distance(transform.position, player.transform.position) < 0.5f)
            {
                GameObject.Find("Canvas").GetComponentInChildren<ExperiencePointsDisplay>().addPoints(1);
                Destroy(this.gameObject);
            }
           


        }

    }
    /*void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.collider.tag == "Player")
        {
            print("Im destroying");

        }
    }*/
}
