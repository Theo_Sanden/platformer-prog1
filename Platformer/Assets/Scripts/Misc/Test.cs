﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {
   
	// Use this for initialization
	void Start () {
        for (int i = 0; i < 10; i++)
        {
            Instantiate(Resources.Load("ExpPoints"));
        }
	}
	
	// Update is called once per frame
	void Update () {
        Instantiate(Resources.Load("ExpPoints"),transform.position, Quaternion.identity);
    }
}
